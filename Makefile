CFLAGS = -O2 -Wall 
DEFS = -D_XOPEN_SOURCE -D_BSD_SOURCE -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE
LIBS = -lrt  -lsqlite3 -lcurl -ldl -lpthread 
#-L /usr/local/lib/mysql 
CC = gcc

#LIBDIRS= -L wherever

DEPS  = ydefs.h Makefile
SRCS = scan.c globs.c http.c main.c tune.c dvr.c epg.c recorder.c
OBJS = obj/scan.o obj/globs.o obj/http.o obj/main.o obj/tune.o obj/dvr.o obj/epg.o obj/recorder.o


obj/%.o: %.c $(DEPS)
	$(CC) $(DEFS) $(CFLAGS)  -c $<  -o $@

#obj/%.o: epg/%.cpp 
#	$(CC) $(DEFS) $(CFLAGS)  -c $<  -o $@


v2Yahdr: $(OBJS) $(DEPS)
	$(CC) -o v2Yahdr $(OBJS) $(LIBDIRS) $(LIBS)





clean: 
	rm obj/*.o v2Yahdr

reallyclean:
	clean
	rm *~