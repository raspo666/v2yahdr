<?
/*************************************************************************
 * Copyright 2009/2010/2011 Ralph Spitzner (rasp@spitzner.org)
 *
 * This file is part of v2Yahdr.
 *
 * v2Yahdr is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Yahdr is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with v2Yahdr.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

$yserv = "http://localhost:4000";
$realserv = "http://10.0.68.1:4000";

?>