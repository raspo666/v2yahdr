<?
/*************************************************************************
 * Copyright 2009/2010/2011 Ralph Spitzner (rasp@spitzner.org)
 *
 * This file is part of v2Yahdr.
 *
 * v2Yahdr is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Yahdr is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with v2Yahdr.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

$name = $_GET['name'];

?>

<html>
<title>v2Yahdr - EPG for <?echo $name?></title>
<head>
<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body style="font-family:Arial,sans-serif;">
<table border="0">
<?
  include "globs.php";





$f = fopen("$yserv/getepg=$name&start=now","r");
$blob = stream_get_contents($f);

$recs = explode("@NR@",$blob);




foreach($recs as $record)
  {
  $data  = explode("@SEP@",$record);
  echo "<tr><th valign=\"top\">";
  echo date("D. d-m G:i",$data[2]);
  echo "<br>".($data[3]/60)."&nbsp;min.<br>";
  echo "<a href=\"$realserv/record=$name@SEP@$data[4]@SEP@$data[2]@SEP@$data[3]\">[record]</a>";
  echo "</th><th valign=\"top\">";
  echo "<strong>".$data[4]."</strong><br>";
  if(strlen($data[5]))
    {
      echo "(".$data[5].")<br>";
    }
  echo "<hr>";
  echo str_replace("\n","<br>",$data[6])."</th>";
  echo "</tr>";
  }






?>
</body>
</html>
