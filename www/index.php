<?
/*************************************************************************
 * Copyright 2009/2010/2011 Ralph Spitzner (rasp@spitzner.org)
 *
 * This file is part of v2Yahdr.
 *
 * v2Yahdr is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Yahdr is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with v2Yahdr.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/
/**************************************************************************

Files in this directory 'talk' to v2Yahdr via http.
Put them somewhere where only your localnet hast acess to, preferably
the same host as v2Yahdr runs on, as we then can simply connect to 
'localhost'.

Some sort of storage for eg. a 'Favourites' list is needed,
sqlite comes in handy here ;-)

These files are the part you should modify to suit your need's,
if you don't want to poke around the C-source.

As I'm not a php/html/css  genius my example files here will look ugly 
and/or clumsy at best, feel free to help :-)

***************************************************************************/


include "globs.php";


$fav = file("./favourites.txt",FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);


function set_channel($name)
{
  global $yserv;


  echo "call:$yserv/tuneto=$name";
  $f = fopen("$yserv/tuneto=$name","r");
  if($f == 0)
    return;
  $res = stream_get_contents($f);
  echo "result: ".$res."<br>";

  fclose($f);
}

function get_nownext($name)
{
  global $yserv;

  $f = fopen("$yserv/nownext=$name","r");
  if($f == 0)
    {
      echo "URLopen failed @ get_nownext()<br>";
      return;
    }
  $blob = stream_get_contents($f);
  $recs = explode("@NR@",$blob);
   
  return $recs;
}



?>

<html>
<title>v2Yahdr - Start page</title>
<head>
<link rel="stylesheet" type="text/css" href="style.css" />
<meta http-equiv="refresh" content="300;url=<?echo $_SERVER['PHP_SELF']."?".time(0)?>"> 

<script language="javascript">

function epgwin(name)
{
  url = "http://10.0.68.1/v2y/showepg.php?name="+name;

  window.open(url,"EPG for "+name,
	      "menubar=no,width=600,height=500,toolbar=no,scrollbars=yes");

}

function toggle(name)
{

  me = document.getElementById(name);


  //alert("style is:"+me.style.display);

  if(me.style.display == "none")
    {

      me.style.display = "block";
    }
  else
    {
      me.style.display = "none";
    }
}



</script>
</head>
<body style="font-family:Arial,sans-serif;">
<?
  if($_GET["set"])
    {
      set_channel($_GET["set"]);
    }


echo "<table border=\"0\" width=\"100%\">";
  
$p = 0;

foreach($fav as $prog)
{
  echo "<tr>";
  echo "<th align=\"middle\" valign=\"top\" width=\"10%\">";
  echo "<br><center><a href=\"".$_SERVER['PHP_SELF']."?set=$prog\">$prog</a><br><br>";
  echo "<a href=\"javascript:epgwin('$prog')\">EPG</a>";
  echo "</center></th>";
  $recs = get_nownext($prog);
  for($i = 0; $i != 2; $i++)
    {
      $data  = explode("@SEP@",$recs[$i]);
      echo "<th width=\"45%\"valign=\"top\">";
      echo "<table><tr><td valign=\"top\">";
      echo date("G:i",$data[1]);
      echo "<br>".($data[2]/60)."&nbsp;min.";
      echo "</th><td valign=\"top\" style=\"font-size:90%;\">";
      echo "<strong>".$data[3]."</strong><br>";
      if(strlen($data[4]))
	{
	  echo "(".$data[4].")<br>";
	}
      //echo "<hr>";
	if(strlen($data[5]))
	  {
        echo "<div onclick=\"toggle('desc$p');\" style=\"cursor: pointer;\">--></div><div id=\"desc$p\" onclick=\"javascript:toggle('$desc$p')\" style=\"display: none\">";
	echo str_replace("\n","<br>",$data[5])."</div></th>";
	  }
      echo "</tr></table>";
      $p++;
    }
  echo "</tr>";

}


?>
</table>
</body>
</html>
