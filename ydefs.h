/*************************************************************************
 * Copyright 2009/2010/2011 Ralph Spitzner (rasp@spitzner.org)
 *
 * This file is part of v2Yahdr.
 *
 * v2Yahdr is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * v2Yahdr is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with v2Yahdr.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/


/***
This space intentionally left blnk for your notes.





***/



#ifndef YDEFS_H
#define YDEFS_H

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <errno.h>
#include <signal.h>
#include <assert.h>
#include <glob.h>
#include <ctype.h>
#include <stdint.h>
#include <sys/select.h>
#include <mqueue.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <pthread.h>
#include <linux/dvb/frontend.h>
#include <linux/dvb/dmx.h>

#include <sqlite3.h>

/// string conversion in http.c
#include <curl/curl.h>
#include <iconv.h>



typedef unsigned char bool;

#define true 1
#define false 0


// return values for checksec ()

#define S_WANT 1
#define S_HAVE 2
#define S_DONE 3



#define ABUSY  1
#define AIDLE  0

#define CABLE  1
#define TERR   2
#define SAT    3  //not yet

/// adapter->pref
#define WANT_EPG  0x01
#define HAS_CA    0x04


/***
have some path like '~/.v2yahdr/' here
***/

#define DBNAME "v2yahdr-db.sq3"


#define HTTP_PORT  4000
#define DVR_PORT   6010




#define MSG_SIZE 8192

#define DEBUG

#ifdef DEBUG
#define INFO(args...) printf( args )
#else
#define INFO(args...)
#endif



/// structs for controlling the adapters

typedef struct adapter
    {
    uint8_t type;
    /**
    number = dvr & frontend adapter
    demux = demux adapter
    these are seperate, because if you're using
    dvbloopback for 'some' :-P reason reading thru
    the loop demux is slow and sometimes garbles data.....
    **/
    uint8_t number;
    uint8_t demux;
    uint8_t pref;
    uint8_t watching;
    uint8_t busy; /// scheduled for recording, or someone's watching
    int     fe_fd;  /// can be open once
    struct adapter *next;
    } ADAPTER;


typedef struct ts
    {
    uint16_t tsid;
    uint32_t freq;
    uint8_t  qam;
    uint8_t  bw;
    uint8_t  adapter; //refers to adapter # in db
    struct ts *next;
    } TRANSPORT;

typedef struct apid
    {
    uint16_t ppid;  //program PID (elementary PID)
    uint16_t tsid;  //same as in SERVICE  (both refer to 'parent')
    uint16_t pid;
    uint8_t  type;
    uint8_t  ac3; // 1 if ac3
    uint8_t  lang[4]; // i.e. "GER" -> padded 0
    struct apid *next;
    } APID;

typedef struct prg
    {
    uint16_t transponder; //internal #
    uint16_t s_id; // service id
    uint8_t  eit;
    uint8_t  pf;
    uint8_t service_type;
    uint16_t ppid;  //program PID (elementary PID)
    uint16_t tsid;  //same as in SERVICE (transport stream id)
    uint16_t vpid;
    struct prg *next;
    } PRG;

typedef struct serv
    {
    uint8_t running; //  1 == not running (probably no elementary PID on transponder at time of scan)
    uint8_t type;   // service type 1 = television.....
    uint16_t tsid;
    uint16_t s_id;
    uint8_t ca;
    char provider[255];
    char name[255];
    struct serv *next;
    } SERVICE;



/***
this one gets filled by tuneto(),
to easyly get info if someone is
watching=1
***/

typedef struct tuned
{
  ADAPTER   *adp;
  TRANSPORT *tsp;
  PRG       *prgp;
  APID      *apidp;
  SERVICE   *sp;
}TUNED;



/// for eit/epg

typedef struct scontrol
    {
    uint16_t s_id;
    uint8_t  version; /// version number supplied by multiplex
    uint8_t  lastsec;
    uint8_t  last_table_id;
    uint8_t  sections[16][255];
    uint8_t  done; /// set to 1 if all sections for table/s_id collected
    struct scontrol *next;
    } SCONTROL;


typedef struct event
    {
    uint16_t tsid;
    uint16_t s_id;
    uint16_t event_id;
    uint64_t start_time; /// in seconds
    uint16_t duration;
    char     name[255];
    char     short_d[255];
    char     long_d[4096];
    struct event *next;
    } EVENT;

/***
abstract of services for EPG corelation
***/

typedef struct slist
    {
    uint16_t tsid;
    uint16_t s_id;
    uint8_t  ca;
    char name[255];
    struct slist *next;
    } SLIST;




///main.c
extern sqlite3 *db,*epg_db,*rec_db;

extern sqlite3_stmt *stmt;

extern int rc;
extern int col, cols;
extern const char *txt;
extern ADAPTER   *adapters;
extern SERVICE   *services;
extern TRANSPORT *transports;
extern APID      *apid_l;
extern PRG       *programs;
extern TUNED     tuned_ts;
extern pthread_t tspipepid;




/// globs
void specialchars(uint8_t *data,uint8_t size);
int input_timeout (int filedes, unsigned int seconds);
int time_read(int fd,int timeout,uint8_t *buffer,int len);
int set_nonblock(int socket);
#define bcdtoint(i) ((((i & 0xf0) >> 4) * 10) + (i & 0x0f))
time_t convert_date(char *dvb_buf);
void escape_hy(char *source,char *target, int maxlen);

/// http.c

void *httpd(void *);


/// dvr

void *dvrd(void *);
void *tspipe(void *);
extern volatile uint16_t pids[10];
extern volatile uint8_t cdvr;
/// tune.c

bool tuneto(char *name);

/** scan.c */
bool check_frontend (int fe_fd,uint16_t maxtry);
int scan(ADAPTER *);
/** epg.c **/
#define EPG_DBNAME "v2yahdr-epg-db.sq3"
#define EPGSLEEP 2*60*60  /// interval between epg read attempts
#define EPGMAXWAIT 60  /// timeout for do_data
#define DO_EPG  0

void *epg_runner(void *);

/** recorder.c **/

#define REC_DBNAME "v2yahdr-rec-db.sq3"
char *sched_rec(FILE *talk,char *what);


#endif /* YDEFS_H */
