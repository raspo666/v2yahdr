/*************************************************************************
 * Copyright 2009/2010/2011 Ralph Spitzner (rasp@spitzner.org)
 *
 * This file is part of v2Yahdr.
 *
 * v2Yahdr is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Yahdr is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with v2Yahdr.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

#include "ydefs.h"

/***
some globals
***/
sqlite3 *db,*epg_db,*rec_db;
sqlite3_stmt *stmt;

int rc;
int col, cols;
const char *txt;

ADAPTER   *adapters;
SERVICE   *services;
TRANSPORT *transports;
APID      *apid_l;
PRG       *programs;
TUNED     tuned_ts;



int
main(int ac,char **av)
    {
    pthread_t kinder[5];
    char buffer[255];
    int num;
    ADAPTER *adp;
    SERVICE *esp;
    APID    *apidp;
    PRG     *prgp;
    TRANSPORT *tsp;

    char *txt;

    int i;


    adapters = adp = NULL;
    services = esp = NULL;
    apid_l = apidp = NULL;
    programs = prgp = NULL;
    transports = tsp = NULL;
    cdvr = 255;

    bzero(&tuned_ts,sizeof(TUNED));



    curl_global_init(0);
    rc = sqlite3_open(DBNAME,&db);
    if(rc)
        {
        fprintf(stderr, "Can't open database: %s @ %d %s\n", sqlite3_errmsg(db),__LINE__,__FILE__);
        return false;
        }
    rc = sqlite3_open(EPG_DBNAME,&epg_db);
    if(rc)
        {
        fprintf(stderr, "Can't open database: %s @ %d %s\n", sqlite3_errmsg(epg_db),__LINE__,__FILE__);
        return false;
        }
    rc = sqlite3_open(REC_DBNAME,&rec_db);
    if(rc)
        {
        fprintf(stderr, "Can't open database: %s @ %d %s\n", sqlite3_errmsg(rec_db),__LINE__,__FILE__);
        return false;
        }


/***
read in availabe adapters
****/

    sprintf(buffer,"select * from adapters order by pref desc");
    rc = sqlite3_prepare_v2(db, buffer, -1, &stmt, 0);
    if( rc )
        {
        fprintf(stderr, "SQL error: %d : %s @ %s %d\n", rc, sqlite3_errmsg(db),__FILE__,__LINE__);
        exit(0);
        }
    else
        {
        i = 0;
        cols = sqlite3_column_count(stmt);
        // execute the statement
        do
            {
            rc = sqlite3_step(stmt);
            switch( rc )
                {
                case SQLITE_DONE:
                    break;
                case SQLITE_ROW:
                    if(adapters == NULL)
                        {
                        if(!(adp = adapters = malloc(sizeof(ADAPTER))))
                            {
                            perror("malloc adapters");
                            exit(0);
                            }
                        bzero(adapters,sizeof(ADAPTER));
                        }
                    else
                        {
                        if(!(adp->next = malloc(sizeof(ADAPTER))))
                            {
                            perror("malloc adapters");
                            exit(0);
                            }
                        adp = adp->next;
                        bzero(adp,sizeof(ADAPTER));
                        }

                    adp->type = sqlite3_column_int(stmt, 0);
                    adp->pref = sqlite3_column_int(stmt, 1);
                    adp->number = sqlite3_column_int(stmt, 2);
                    adp->demux  = sqlite3_column_int(stmt, 3);
                    printf("found %d \n",adp->number);
                    break;
                default:
                    fprintf(stderr, "Error: %d : %s\n",  rc, sqlite3_errmsg(db));
                    break;
                }

            }
        while( rc==SQLITE_ROW );
        // finalize the statement to release resources
        sqlite3_finalize(stmt);
        }

/***
read prgs
***/

    sprintf(buffer,"select * from prg");
    rc = sqlite3_prepare_v2(db, buffer, -1, &stmt, 0);
    if( rc )
        {
        fprintf(stderr, "SQL error: %d : %s @ %s %d\n", rc, sqlite3_errmsg(db),__FILE__,__LINE__);
        exit(0);
        }
    else
        {
        do
            {
            rc = sqlite3_step(stmt);
            switch( rc )
                {
                case SQLITE_DONE:
                    break;
                case SQLITE_ROW:
                    if(programs == NULL)
                        {
                        if(!(prgp = programs = malloc(sizeof(PRG))))
                            {
                            perror("malloc programs");
                            exit(0);
                            }
                        bzero(programs,sizeof(PRG));
                        }
                    else
                        {
                        if(!(prgp->next = malloc(sizeof(PRG))))
                            {
                            perror("malloc programs");
                            exit(0);
                            }
                        prgp = prgp->next;
                        bzero(prgp,sizeof(PRG));
                        }

                    prgp->s_id = sqlite3_column_int(stmt, 0);
                    prgp->service_type = sqlite3_column_int(stmt, 1);
                    prgp->eit = sqlite3_column_int(stmt, 2);
                    prgp->pf  = sqlite3_column_int(stmt, 3);
                    prgp->ppid  = sqlite3_column_int(stmt, 4);
                    prgp->tsid  = sqlite3_column_int(stmt, 5);
                    prgp->vpid  = sqlite3_column_int(stmt, 6);
                    /// don't need e_vers


                    break;
                default:
                    fprintf(stderr, "Error: %d : %s\n",  rc, sqlite3_errmsg(db));
                    break;
                }

            }
        while( rc==SQLITE_ROW );
        // finalize the statement to release resources
        sqlite3_finalize(stmt);
        }

/***
read transports
***/
    sprintf(buffer,"select * from ts order by adapter desc");
    rc = sqlite3_prepare_v2(db, buffer, -1, &stmt, 0);
    if( rc )
        {
        fprintf(stderr, "SQL error: %d : %s @ %s %d\n", rc, sqlite3_errmsg(db),__FILE__,__LINE__);
        exit(0);
        }
    else
        {
        do
            {
            rc = sqlite3_step(stmt);
            switch( rc )
                {
                case SQLITE_DONE:
                    break;
                case SQLITE_ROW:
                    if(transports == NULL)
                        {
                        if(!(tsp = transports = malloc(sizeof(TRANSPORT))))
                            {
                            perror("malloc transports");
                            exit(0);
                            }
                        bzero(transports,sizeof(TRANSPORT));
                        }
                    else
                        {
                        if(!(tsp->next = malloc(sizeof(TRANSPORT))))
                            {
                            perror("malloc transports");
                            exit(0);
                            }
                        tsp = tsp->next;
                        bzero(tsp,sizeof(TRANSPORT));
                        }

                    tsp->tsid = sqlite3_column_int(stmt, 0);
                    tsp->freq = sqlite3_column_int(stmt, 1);
                    tsp->qam = sqlite3_column_int(stmt, 2);
                    tsp->bw  = sqlite3_column_int(stmt, 3);
                    tsp->adapter  = sqlite3_column_int(stmt, 4);
                    break;
                default:
                    fprintf(stderr, "Error: %d : %s\n",  rc, sqlite3_errmsg(db));
                    break;
                }

            }
        while( rc==SQLITE_ROW );
        // finalize the statement to release resources
        sqlite3_finalize(stmt);
        }



/***
read audio pids
***/
    sprintf(buffer,"select * from apid");
    rc = sqlite3_prepare_v2(db, buffer, -1, &stmt, 0);
    if( rc )
        {
        fprintf(stderr, "SQL error: %d : %s @ %s %d\n", rc, sqlite3_errmsg(db),__FILE__,__LINE__);
        exit(0);
        }
    else
        {
        do
            {
            rc = sqlite3_step(stmt);
            switch( rc )
                {
                case SQLITE_DONE:
                    break;
                case SQLITE_ROW:
                    if(apid_l == NULL)
                        {
                        if(!(apid_l = apidp = malloc(sizeof(APID))))
                            {
                            perror("malloc apids");
                            exit(0);
                            }
                        bzero(apid_l,sizeof(APID));
                        }
                    else
                        {
                        if(!(apidp->next = malloc(sizeof(APID))))
                            {
                            perror("malloc apids");
                            exit(0);
                            }
                        apidp = apidp->next;
                        bzero(apidp,sizeof(APID));
                        }

                    apidp->ppid = sqlite3_column_int(stmt, 0);
                    apidp->tsid = sqlite3_column_int(stmt, 1);
                    apidp->pid = sqlite3_column_int(stmt, 2);
                    apidp->ac3  = sqlite3_column_int(stmt, 3);
                    apidp->type  = sqlite3_column_int(stmt, 4);
                    txt = (char *)sqlite3_column_text(stmt, 5);
                    strcpy((char *)apidp->lang,(char *)txt);

                    break;
                default:
                    fprintf(stderr, "Error: %d : %s\n",  rc, sqlite3_errmsg(db));
                    break;
                }

            }
        while( rc==SQLITE_ROW );
        sqlite3_finalize(stmt);
        }



/***
read in services
***/

   sprintf(buffer,"select * from serv order by name");
    rc = sqlite3_prepare_v2(db, buffer, -1, &stmt, 0);
    if( rc )
        {
        fprintf(stderr, "SQL error: %d : %s @ %s %d\n", rc, sqlite3_errmsg(db),__FILE__,__LINE__);
        exit(0);
        }
    else
        {
        i = 0;
        cols = sqlite3_column_count(stmt);
        // execute the statement
        do
            {
            rc = sqlite3_step(stmt);
            switch( rc )
                {
                case SQLITE_DONE:
                    break;
                case SQLITE_ROW:
                    if(services == NULL)
                        {
                        if(!(esp = services = malloc(sizeof(SERVICE))))
                            {
                            perror("malloc services");
                            exit(0);
                            }
                        bzero(services,sizeof(SERVICE));
                        }
                    else
                        {
                        if(!(esp->next = malloc(sizeof(SERVICE))))
                            {
                            perror("malloc services");
                            exit(0);
                            }
                        esp = esp->next;
                        bzero(esp,sizeof(SERVICE));
                        }
                    esp->type = sqlite3_column_int(stmt, 0);
                    esp->tsid = sqlite3_column_int(stmt, 1);
                    esp->s_id = sqlite3_column_int(stmt, 2);
                    txt = (char *)sqlite3_column_text(stmt, 3);
                    strcpy(esp->provider,(char *)txt);
                    txt = (char *)sqlite3_column_text(stmt, 4);
                    strcpy(esp->name,(char *)txt);
                    esp->ca = sqlite3_column_int(stmt, 5);
                    break;
                default:
                    fprintf(stderr, "Error: %d : %s\n",  rc, sqlite3_errmsg(db));
                    break;

                }
            }
        while( rc==SQLITE_ROW );
        sqlite3_finalize(stmt);
        }
    if(ac == 3)
        {
        if(!strcmp(av[1],"-scan"))
            {
            num = atoi(av[2]);
            if(num == 0 && strcmp(av[2],"0"))
                {
                printf("cannot use adapter >%s<\n",av[2]);
                exit(0);
                }
            /// else it's really 0 :-P
            adp = adapters;
            while(adp)
                {
                if(adp->number == num)
                    {
                    scan(adp);
                    exit(0);
                    }
                adp = adp->next;
                }






            if(adp == NULL)
                {
                printf("adapter %s not found\n",av[2]);
                exit(0);
                }

            }
        }

    pthread_create(&kinder[0], NULL, dvrd, NULL);
    pthread_create(&kinder[1], NULL, httpd, NULL);
    pthread_create(&kinder[2], NULL, tspipe, NULL);
    pthread_create(&kinder[3],NULL,epg_runner,NULL);
    //pthread_create(&kinder[4],NULL,rec_runner,NULL);
    /// Ali gehen andere Baustelle
    while(1)
      {
	sleep(30);
	for(i = 0; i != 3 ; i++)
	  {
	    if(pthread_kill(kinder[i],0) != 0)
	      {
		printf("son #%d has left us ",i);
		perror("");
	      }
	  }
      }
    exit(0);


    }
