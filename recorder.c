/*************************************************************************
 * Copyright 2009/2010/2011 Ralph Spitzner (rasp@spitzner.org)
 *
 * This file is part of v2Yahdr.
 *
 * v2Yahdr is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Yahdr is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with v2Yahdr.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/

/***
recording thread always has precedence when tuning
***/

#include "ydefs.h"

static sqlite3_stmt *rec_stmt;
static int rec_rc;


bool recorder_tune(char *name)
    {
    /**
    Shaft, can you dig it? - Isaac hayes
    **/
    }


/***
four entrys per recording in db
start time,duration, channel, name of broadcast...
***/


/***
'what' looks like:
kabel eins@SEP@Quincy@SEP@1304576820@SEP@3600

talk is backchannel to http
*/

char *sched_rec(FILE *talk,char *what)
    {
    char *station,*bcast,*p,*p2;
    uint64_t start;
    uint16_t dur;
    char sql[1024];


    station = what;
    /***
    Got to keep 'em seperated - Green Day
    ***/
    p = strstr(what,"@SEP@");
    *p = '\0';
    p += 5;
    bcast = p;
    p = strstr(p,"@SEP@");
    *p = '\0';
    p += 5;
    p2 = strstr(p,"@SEP@");
    *p2 = '\0';
    start = atol(p);
    p2 += 5;
    dur = atoi(p2);

    sprintf(sql,"insert into recordings values ('%s','%s','%lld','%d')",
            station,bcast,start,dur);
    sqlite3_prepare_v2(rec_db, sql, -1, &rec_stmt, 0);
    sqlite3_step(rec_stmt);
    sqlite3_finalize(rec_stmt);



    }


/**
check rec_db every 4 minutes,
start recordings if somethings pending
**/

void *rec_runner(void *laber)
    {
    char sql[1024];
    time_t now;
    const unsigned char station,bcast;
    time_t start;
    uint16_t dur;

    while(1)
        {

        sleep(240);
        }
    }
