/*************************************************************************
 * Copyright 2009/2010/2011 Ralph Spitzner (rasp@spitzner.org)
 *
 * This file is part of v2Yahdr.
 *
 * v2Yahdr is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Yahdr is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with v2Yahdr.  If not, see <http://www.gnu.org/licenses/>.
 **************************************************************************/


#include "ydefs.h"



/***
throw away control chars
probably shoul hono charset....
***/

void
specialchars(uint8_t *data,uint8_t size)
    {
    uint8_t i,i2;

    /*    if(!(buffer = malloc(size)))
            {
            printf("mem %s %d",__FILE__,__LINE__);
            perror("");
            }
        bzero(buffer,size);*/

    i = i2 = 0;

    while((i != size) && (data[i] != '\0'))
        {
        if(data[i] < 0x20 || data[i] == 0x86 || data[i] == 0x87 || data[i] == 0x8a)
            {
            i++;
            continue;
            }
        data[i2] = data[i];
        i2++;
        i++;
        }
    data[i2] = '\0';

    }


int set_nonblock(int socket)
    {
    int flags;
    flags = fcntl(socket,F_GETFL,0);
    assert(flags != -1);
    fcntl(socket, F_SETFL, flags | O_NONBLOCK);
    return flags;
    }

void set_block(int socket)
    {
    int flags;
    flags = fcntl(socket,F_GETFL,0);
    assert(flags != -1);
    fcntl(socket, F_SETFL, flags & ~O_NONBLOCK);
    }


int
input_timeout (int filedes, unsigned int seconds)
    {
    fd_set set;
    struct timeval timeout;

    /* Initialize the file descriptor set. */
    FD_ZERO (&set);
    FD_SET (filedes, &set);

    /* Initialize the timeout data structure. */
    timeout.tv_sec = seconds;
    timeout.tv_usec = 0;

    /* select returns 0 if timeout, 1 if input available, -1 if error. */
    return TEMP_FAILURE_RETRY (select (FD_SETSIZE,
                                       &set, NULL, NULL,
                                       &timeout));
    }

/***
maybe a poll() + fcntl(fd, F_SETFL, O_NONBLOCK);
could also do here...

this seems to be under construction forever....
**/

int time_read(int fd,int timeout,uint8_t *buf,int len)
    {
    struct pollfd pfd[1];
    int ret;


    pfd[0].fd = fd;
    pfd[0].events = (POLLIN | POLLPRI);

    //start_time = time(0);
    /*while(1)
      {
        now = time(0);
        if(now - start_time > timeout)
          {
            errno = ETIMEDOUT;
            return -1;
          }
       if(poll())
      }*/

    ret = poll(pfd,1,timeout*1000);
    if (ret<1)
        return -1;

    return read(fd,buf,len);


    }


/***
escape single quotes for sql
****/
void
escape_hy(char *source,char *target, int maxlen)
{

 int i,x;

 for(x = i = 0; x != maxlen; x++)
        {
        if(source[x] == '\0')
          {
            break;
          }
        if(source[i] == '\'')
            {
            target[x] = '\'';
            x++;
            target[x] = '\'';
            }
        else
            {
            target[x] = source[i];
            }
        i++;
        }
      target[x] = '\0';
}


time_t convert_date(char *dvb_buf)
    {
    int i;
    int year, month, day, hour, min, sec;
    long int mjd;
    struct tm dvb_time;

    mjd = (dvb_buf[0] & 0xff) << 8;
    mjd += (dvb_buf[1] & 0xff);
    hour = bcdtoint(dvb_buf[2] & 0xff);
    min = bcdtoint(dvb_buf[3] & 0xff);
    sec = bcdtoint(dvb_buf[4] & 0xff);
    /*
     * Use the routine specified in ETSI EN 300 468 V1.4.1,
     * "Specification for Service Information in Digital Video Broadcasting"
     * to convert from Modified Julian Date to Year, Month, Day.
     */
    year = (int) ((mjd - 15078.2) / 365.25);
    month = (int) ((mjd - 14956.1 - (int) (year * 365.25)) / 30.6001);
    day = mjd - 14956 - (int) (year * 365.25) - (int) (month * 30.6001);
    if (month == 14 || month == 15)
        i = 1;
    else
        i = 0;
    year += i;
    month = month - 1 - i * 12;

    dvb_time.tm_sec = sec;
    dvb_time.tm_min = min;
    dvb_time.tm_hour = hour;
    dvb_time.tm_mday = day;
    dvb_time.tm_mon = month - 1;
    dvb_time.tm_year = year;
    dvb_time.tm_isdst = -1;
    dvb_time.tm_wday = 0;
    dvb_time.tm_yday = 0;
    return (timegm(&dvb_time));
    }


