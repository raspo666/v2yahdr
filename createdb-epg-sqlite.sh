#!/bin/sh


cat | sqlite3 v2yahdr-epg-db.sq3 <<EOF
create table epg (
    tsid  integer,
    s_id  integer,
    event_id integer,
    start_time integer,
    duration integer,
    name text,
    short_des text,
    long_des  text,
    PRIMARY KEY(tsid,s_id,event_id)
);
EOF


