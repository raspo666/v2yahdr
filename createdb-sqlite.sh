#!/bin/sh



cat | sqlite3 v2yahdr-db.sq3 <<EOF




create table if not exists adapters (
type       INTEGER,
pref       INTEGER,
number     INTEGER,
demux      INTEGER);


create table if not exists ts (
tsid       INTEGER,
freq       INTEGER,
qam        INTEGER,
bw         INTEGER,
adapter    INTEGER);

create table if not exists apid (
ppid       INTEGER,
tsid       INTEGER,
pid        INTEGER,
type       INTEGER,
ac3        INTEGER,
lang       TEXT);

create table if not exists prg (
s_id       INTEGER,
service_type       INTEGER,
eit        INTEGER,
pf         INTEGER,
ppid       INTEGER,
tsid       INTEGER,
vpid       INTEGER,
e_vers     INTEGER);

create table if not exists serv (
type       INTEGER,
tsid       INTEGER,
s_id       INTEGER,
provider   TEXT,
name       TEXT,
ca         INTEGER);


EOF

echo fertsch